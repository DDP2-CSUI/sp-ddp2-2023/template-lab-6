import java.util.ArrayList;
import java.util.List;

public class Simulator {
    private List<Customer> customerList = new ArrayList<>();
    private List<DDMarket> marketList = new ArrayList<>();
    public static void main(String[] args) {
        Simulator simulator = new Simulator();
        simulator.init();

        try {
            simulator.run();
        } catch (Exception e) {
            // TODO:
        } finally {
            // TODO:
        }
    }

    private void run() {
        Customer sakura = this.customerList.get(0);
        Customer ganta = this.customerList.get(1);
        Customer cipung = this.customerList.get(2);
        Customer edna = this.customerList.get(3);
        DDMarket astridMarket = this.marketList.get(0);
        DDMarket burhanMarket = this.marketList.get(1);

        sakura.buyProduct(burhanMarket, "Basreng", 1);
        sakura.buyProduct(burhanMarket, "Cimory Squeeze", 3);

        cipung.buyProduct(astridMarket, "Indomie", 20);
        cipung.buyProduct(burhanMarket, "Sosis Kanzler", 25);
        cipung.buyProduct(astridMarket, "Indomie", 10);

        ganta.buyProduct(astridMarket, "Tolak Angin", 14);

        edna.buyProduct(burhanMarket, "Cimory Squeeze", 5);
        edna.buyProduct(burhanMarket, "Basreng", 3);
    }

    private void init() {
        DDMarket astridMarket = new DDMarket("Astrid's Market");
        DDMarket burhanMarket = new DDMarket("Burhan's Market");

        Customer sakura = new Customer("Sakura", 120_000);
        Customer ganta = new Customer("Ganta", 70_000);
        Customer cipung = new CustomerMembership("Rayyanza", 1_500_000);
        Customer edna = new CustomerMembership("Edna", 310_000);

        astridMarket.addProduct(new Product("Pantene", 23_000, 12));
        astridMarket.addProduct(new Product("Indomie", 3500, 50));
        astridMarket.addProduct(new Product("Tolak Angin", 5000, 30));

        burhanMarket.addProduct(new Product("Basreng", 50_000, 10));
        burhanMarket.addProduct(new Product("Sosis Kanzler", 12_000, 25));
        burhanMarket.addProduct(new Product("Cimory Squeeze", 11_000, 70));

        marketList.add(astridMarket);
        marketList.add(burhanMarket);

        customerList.add(sakura);
        customerList.add(ganta);
        customerList.add(cipung);
        customerList.add(edna);
    }
}
